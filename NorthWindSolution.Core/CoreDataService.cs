﻿
namespace NorthWindSolution.Core
{
    /// <summary>
    /// Base data service
    /// </summary>
    public class CoreDataService
    {
        #region Private variables
        /// <summary>
        /// Connection string to access database
        /// </summary>
        private readonly string conString = @"Data Source = DESKTOP-IL04I92;initial catalog=Northwind;Integrated Security=True";
        #endregion

        #region Public Constructor
        public CoreDataService()
        {
            this.ConnectionString = conString;
        }
        #endregion

        #region Properties
        public string ConnectionString { get; set; }    //Get or sets connection string
        #endregion
    }
}
