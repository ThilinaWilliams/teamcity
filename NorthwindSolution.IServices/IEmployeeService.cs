﻿using NorthwindSolution.Entities;
using System.Collections.Generic;


namespace NorthwindSolution.IServices
{
    /// <summary>
    /// Employee service
    /// </summary>
    public interface IEmployeeService
    {
        List<Employees> GetEmployees();
        
    }
}
