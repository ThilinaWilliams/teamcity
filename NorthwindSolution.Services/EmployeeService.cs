﻿using NorthwindSolution.Entities;
using NorthwindSolution.IDataServices;
using NorthwindSolution.IServices;
using System.Collections.Generic;


namespace NorthwindSolution.Services
{
    /// <summary>
    /// Implements employee service
    /// </summary>
    public class EmployeeService : IEmployeeService
    {
        #region Private variables
        private readonly IEmployeeDataService dataService;
        #endregion

        #region Public Constructor
        public EmployeeService(IEmployeeDataService dataService)
        {
            this.dataService = dataService;
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Get all employees
        /// </summary>
        /// <returns>return employee list</returns>
        public List<Employees> GetEmployees()
        {
            return dataService.GetEmployees();
        } 
        #endregion
    }
}
